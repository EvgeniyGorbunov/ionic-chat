import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';


@Injectable()

export class WebscketService {

    private readonly url = 'ws://' + environment.url;
    private websocket: WebSocket;

    constructor() { }

    connect(): WebSocket {
        if (!this.websocket) {
            this.websocket = new WebSocket(this.url);
        }
        return this.websocket;
    }
}
