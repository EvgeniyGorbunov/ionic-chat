import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { User } from 'src/models/user';
import { HttpClient } from '@angular/common/http';

@Injectable()

export class DataService {

    private readonly url = 'http://' + environment.url;

    constructor(private http: HttpClient) { }

    registration(user: any): Promise<string> {
        return this.http.post<string>(this.url.concat('/users'), user).toPromise();
    }

    set userId(id: string) {
        localStorage.setItem('userId', id);
    }
    get userId(): string {
        return localStorage.getItem('userId') || null;
    }
    getUserList(): Promise<User[]> {
        return this.http.get<User[]>(this.url.concat('/users')).toPromise();
    }
}
