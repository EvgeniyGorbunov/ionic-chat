import { Input, Component } from '@angular/core';
import { Message } from 'src/models/message';
import { User } from 'src/models/user';

@Component({
    selector: 'app-message',
    templateUrl: 'message.component.html',
    styleUrls: ['message.component.scss']
})

export class MessageComponent {
    @Input() message: Message;
    @Input() users: User[];


    constructor() { }

    

    public getAvatar(): string {
        const user = this.users.find(u => u.name === this.message.user_name);
        return `assets/avatars/${user.avatar}.png`;
    }
  

    public getIsRight(userId: string): boolean {
        // return this.currentUserId !== userId;
        return false;
    }
}
