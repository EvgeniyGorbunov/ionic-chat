import { NgModule, Component } from '@angular/core';
import { ChatPage } from './chat.page';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { TimePipe } from 'src/pipes/time.pipe';
import { MessageComponent } from './message/message.component';
import { WebscketService } from 'src/services/websocket.serice';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        ChatPage,
        TimePipe,
        MessageComponent
    ],
    imports: [
        CommonModule,
        IonicModule,
        FormsModule,
        RouterModule.forChild([
            {
                path: '',
                component: ChatPage
            }
        ])
    ],
    providers: [
        WebscketService
    ]
})

export class ChatPageModule { }
