import { Component, ViewChild, OnInit } from '@angular/core';
import { Message } from 'src/models/message';
import { User } from 'src/models/user';
import { WebscketService } from 'src/services/websocket.serice';
import { DataService } from 'src/services/data.service';



@Component({
    selector: 'app-chat',
    templateUrl: 'chat.page.html',
    styleUrls: ['chat.page.scss']

})

export class ChatPage implements OnInit {

    private ws: WebSocket;
    public userMessage = '';
    public currentUserId = 'Zhenya';

    public messages: Message[] = [
    ];
    public users: User[] = [
    ];

    @ViewChild('list', { static: false }) public list: any;
    constructor(private websocketService: WebscketService, private dataService: DataService) { }

    ngOnInit() {
        this.loadUserList();
        this.ws = this.websocketService.connect();
        this.ws.onmessage = (request) => {
            const message: Message = JSON.parse(request.data);
            this.messages.push(message);
            this.scrollToBottom();
        };
    }
    private loadUserList(): void {
        this.dataService.getUserList().then(res => {
            this.users = res;
        });
    }




    public scrollToBottom() {
        setTimeout(() => {
            const listElement = this.list.el;
            if (listElement) {
                listElement.scrollTo(0, 10000);
            }
        }, 0);
    }

    public send(): void {
        if (this.userMessage.length > 0) {
            const message: Message = {
                user_name: this.currentUserId,
                text: this.userMessage,
                time: Math.round(new Date().getTime() / 1000)
            };
            this.ws.send(JSON.stringify(message));
            this.userMessage = ('');
            this.scrollToBottom();
        }
    }

}
