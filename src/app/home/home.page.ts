import { Component } from '@angular/core';
import { AuthComponent } from './auth/auth.component';
import { ModalController } from '@ionic/angular';
import { RegistraionComponent } from './registraion/registraion.component';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private modalController: ModalController) { }
  async presentModal() {
    const modal = await this.modalController.create({
      component: AuthComponent
    });

    return await modal.present();
  }
  async peresentRegistration() {
    const modal = await this.modalController.create({
      component: RegistraionComponent
    });

    return await modal.present();
  }


}
