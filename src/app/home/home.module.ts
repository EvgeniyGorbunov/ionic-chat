import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { AuthComponent } from './auth/auth.component';
import { RegistraionComponent } from './registraion/registraion.component';

@NgModule({

  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ]),
    ReactiveFormsModule

  ],
  declarations: [HomePage,
    AuthComponent,
    RegistraionComponent],
  entryComponents: [AuthComponent, RegistraionComponent]
})
export class HomePageModule { }
