import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ModalController, LoadingController } from '@ionic/angular';
import { DataService } from 'src/services/data.service';
import { User } from 'src/models/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registraion',
  templateUrl: './registraion.component.html',
  styleUrls: ['./registraion.component.scss'],
})
export class RegistraionComponent implements OnInit {
  public name: FormControl;
  public password: FormControl;

  public userForm: FormGroup;



  constructor(
    private modalController: ModalController,
    private dataService: DataService,
    private router: Router,
    private loadingController: LoadingController
  ) { }

  ngOnInit() {
    this.createFormFields();
    this.CreateUserForm();
  }

  private createFormFields() {
    this.name = new FormControl('', [
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(32)

    ]);
    this.password = new FormControl('', [
      Validators.required,
      Validators.minLength(6),
      Validators.maxLength(32)]);

  }
  private CreateUserForm() {
    this.userForm = new FormGroup({
      name: this.name,
      password: this.password
    });
  }
  public async closeModal() {
    await this.modalController.dismiss();
  }

  public async register() {
    if (this.userForm.valid) {
      const user = {
        name: this.name.value,
        password: this.password.value,
        avatar: '1'
      };
      const loader = await this.loadingController.create({
        message: 'Подождите...'

      });
      loader.present();
      this.dataService.registration(user)
        .then(res => {
          this.dataService.userId = res;
          this.modalController.dismiss();
          this.router.navigate(['account']);
        })
        .catch(err => { })
        .finally(() => { loader.dismiss() });
    }
  }

}
