import { FormControl } from '@angular/forms';

export function emailValidator(email: FormControl) {
    const value = email.value;
    const emailRegexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const isValid = emailRegexp.test(value);
    if (isValid) {
        return null;
    } else {
        return {
            emailValidator: {
                valid: false
            }
        };
    }

}
