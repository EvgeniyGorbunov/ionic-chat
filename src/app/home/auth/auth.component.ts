import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { emailValidator } from '../homeValidator';

@Component({
    selector: 'app-auth',
    templateUrl: 'auth.component.html',
    styleUrls: ['auth.component.scss']
})

export class AuthComponent implements OnInit {
    public name: FormControl;
    public password: FormControl;

    public userForm: FormGroup;



    constructor(private modalController: ModalController) { }

    ngOnInit() {
        this.createFormFields();
        this.CreateUserForm();
    }

    private createFormFields() {
        this.name = new FormControl('', [
            Validators.required,
            Validators.minLength(5),
            Validators.maxLength(32)

        ]);
        this.password = new FormControl('', [
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(32)]);

    }
    private CreateUserForm() {
        this.userForm = new FormGroup({
            name: this.name,
            password: this.password
        });
    }
    public async closeModal() {
        await this.modalController.dismiss();
    }

}
