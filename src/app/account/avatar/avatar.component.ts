import { Component, Input, Output, EventEmitter } from "@angular/core";

@Component({
    selector: 'app-avatar',
    templateUrl: 'avatar.component.html',
    styleUrls: ['avatar.component.scss']
})

export class AvatarComponent {

    static selectedAvatar: string;
    @Input() currentAvatar: string;
    @Output() changeAvatar = new EventEmitter<string>();

    constructor() { }

    public get isSelected(): boolean {
        return AvatarComponent.selectedAvatar === this.currentAvatar;
    }
    public selectAvatar(): void {
        AvatarComponent.selectedAvatar = this.currentAvatar;
        this.changeAvatar.emit(this.currentAvatar);
    }

    public get avatarSrc(): string {
        return `assets/avatars/${this.currentAvatar}.png`;
    }
}
