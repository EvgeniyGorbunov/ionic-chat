import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/models/user';
import { AvatarComponent } from './avatar/avatar.component';
import { Location } from '@angular/common';

@Component({
    selector: 'app-account-page',
    templateUrl: 'account.page.html',
    styleUrls: ['account.page.scss']
})

export class AccountPage implements OnInit {
    public name: FormControl;
    public password: FormControl;

    public userForm: FormGroup;
    private user: User = {
        id: 0,
        avatar: '1',
        name: 'UserPig',
        password: '123456'
    };

    public avatars: string[] = ['1', '2', '3', '4', '5', '6'];

    constructor(private location: Location) { }

    ngOnInit() {
        AvatarComponent.selectedAvatar = this.user.avatar;
        this.createFormFields();
        this.createUserForm();

    }

    public createFormFields(): void {
        this.name = new FormControl(this.user.name, [
            Validators.required,
            Validators.maxLength(3),
            Validators.maxLength(15)
        ]);
        this.password = new FormControl(this.user.password, [
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(20) // TODO async validator
        ]);
    }

    public createUserForm(): void {
        this.userForm = new FormGroup({
            name: this.name,
            password: this.password
        });
    }

    public cancel(): void {
        this.location.back();
    }

    public onAvatarChange(avatar: string): void {
        this.user.avatar = avatar;
        console.log(this.user);
    }



}
