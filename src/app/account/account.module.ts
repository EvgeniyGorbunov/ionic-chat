import { NgModule } from '@angular/core';
import { AccountPage } from './account.page';
import { IonicModule } from '@ionic/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AvatarComponent } from './avatar/avatar.component';

@NgModule({
    declarations: [AccountPage, AvatarComponent],
    imports: [
        IonicModule,
        ReactiveFormsModule,
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: AccountPage
            }
        ])
    ]
})

export class AccountPageModule {

}
