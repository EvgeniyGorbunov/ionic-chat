export interface Message {
    time: number;
    text: string;
    user_name: string;
}
