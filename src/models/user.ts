export interface User {
    email?: string;
    password?: string;
    id?: number;
    avatar: string;
    name: string;
}
